const path = require('path')
const CompressionPlugin = require('compression-webpack-plugin')
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: './',
  productionSourceMap: false,
  configureWebpack: () => {
    let config = {
      resolve: {
        alias: {
          '@S': resolve('./src'),
        },
      },
      module: {
        rules: [
          {
            test: /\.css/,
            use: ['style-loader', 'css-loader', 'postcss-loader'],
          },
        ],
      },
      plugins: [
        new CompressionPlugin({
          test: /\.js$|\.html$|\.css$|\.jpg$|\.jpeg$|\.png/, // 需要压缩的文件类型
          threshold: 10240, // 归档需要进行压缩的文件大小最小值，我这个是10K以上的进行压缩
          deleteOriginalAssets: false, // 是否删除原文件
        }),
      ],
    }
    if (process.env.NODE_ENV === 'production') {
      return config
    } else {
      return config
    }
  },
  devServer: {
    port: 4000,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_SERVER_URL,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
      '/userfiles': {
        target: process.env.VUE_APP_SERVER_File,
        changeOrigin: true,
        pathRewrite: {
          '^/userfiles': '/userfiles',
        },
      },
    },
  },
}
