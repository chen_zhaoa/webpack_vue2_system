import Vue from 'vue'
import VueRouter from 'vue-router'
const _import = (file) => () => import('@S/views/' + file + '.vue')
Vue.use(VueRouter)
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((error) => error)
}
const routes = [
  {
    path: '/',
    name: 'Main',
    component: _import('layout/Main'),
  },
]

const router = new VueRouter({
  mode: 'hash',
  routes,
})
// router.beforeEach((to, from, next) => {
// })
export default router
