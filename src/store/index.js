import Vue from 'vue'
import Vuex from 'vuex'
import common from './modules/common'
import user from './modules/user'
import config from './modules/config'
Vue.use(Vuex)

export default (storeModules) => {
  let modules = {
    common,
    user,
    config,
  }
  if (storeModules) {
    modules = {
      ...modules,
      ...storeModules,
    }
  }
  return new Vuex.Store({
    state: {},
    modules: modules,
    mutations: {},
    strict: process.env.NODE_ENV !== 'production',
  })
}
