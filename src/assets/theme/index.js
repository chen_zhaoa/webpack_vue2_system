function mix(c1, c2, p) {
  let ratio = p
  ratio = 1 - parseInt(ratio) / 100
  ratio = Math.max(Math.min(Number(ratio), 1), 0)
  let r1 = parseInt(c1.substring(1, 3), 16)
  let g1 = parseInt(c1.substring(3, 5), 16)
  let b1 = parseInt(c1.substring(5, 7), 16)
  let r2 = parseInt(c2.substring(1, 3), 16)
  let g2 = parseInt(c2.substring(3, 5), 16)
  let b2 = parseInt(c2.substring(5, 7), 16)
  let r = Math.round(r1 * (1 - ratio) + r2 * ratio)
  let g = Math.round(g1 * (1 - ratio) + g2 * ratio)
  let b = Math.round(b1 * (1 - ratio) + b2 * ratio)
  r = ('0' + (r || 0).toString(16)).slice(-2)
  g = ('0' + (g || 0).toString(16)).slice(-2)
  b = ('0' + (b || 0).toString(16)).slice(-2)
  return '#' + r + g + b
}
export function getColors(store) {
  const defaultTheme = store.state.config.defaultTheme || '#409EFF'
  // 定义默认颜色
  const white = '#ffffff'
  const black = '#000000'
  const success = '#0fb336'
  const warning = '#eb9e05'
  const danger = '#e70000'
  const info = '#878d99'
  // 定义css变量
  const colors = {
    '--color-primary': defaultTheme,
    '--color-success': success,
    '--color-warning': warning,
    '--color-danger': danger,
    '--color-info': info,
    '--color-white': white,
    '--color-black': black,
    '--color-success-light': mix(white, success, '80%'),
    '--color-warning-light': mix(white, warning, '80%'),
    '--color-danger-light': mix(white, danger, '80%'),
    '--color-info-light': mix(white, info, '80%'),
    '--color-success-lighter': mix(white, success, '90%'),
    '--color-warning-lighter': mix(white, warning, '90%'),
    '--color-danger-lighter': mix(white, danger, '90%'),
    '--color-info-lighter': mix(white, info, '90%'),
    '--slider-button-hover-color': mix(defaultTheme, black, '97%'),
    '--color-white-primary-92\\%': mix(white, defaultTheme, `92%`),
  }
  for (let i = 0; i < 11; i++) {
    if (i === 0) {
      colors[`--color-primary-light-${i}`] = mix(white, defaultTheme, `${i}0%`)
      colors[`--color-primary-white-${i}\\%`] = mix(
        defaultTheme,
        white,
        `${i}0%`,
      )
      colors[`--color-info-white-${i}\\%`] = mix(info, white, `${i}0%`)
      colors[`--color-success-white-${i}\\%`] = mix(success, white, `${i}0%`)
      colors[`--color-warning-white-${i}\\%`] = mix(warning, white, `${i}0%`)
      colors[`--color-danger-white-${i}\\%`] = mix(warning, white, `${i}0%`)
      colors[`--color-black-primary-${i}\\%`] = mix(
        black,
        defaultTheme,
        `${i}0%`,
      )
      colors[`--color-black-success-${i}\\%`] = mix(black, success, `${i}0%`)
      colors[`--color-black-warning-${i}\\%`] = mix(black, warning, `${i}0%`)
      colors[`--color-black-danger-${i}\\%`] = mix(black, danger, `${i}0%`)
      colors[`--color-black-info-${i}\\%`] = mix(black, info, `${i}0%`)
      colors[`--color-white-primary-${i}\\%`] = mix(
        white,
        defaultTheme,
        `${i}0%`,
      )
      colors[`--color-white-success-${i}\\%`] = mix(white, success, `${i}0%`)
      colors[`--color-white-warning-${i}\\%`] = mix(white, warning, `${i}0%`)
      colors[`--color-white-danger-${i}\\%`] = mix(white, danger, `${i}0%`)
      colors[`--color-white-info-${i}\\%`] = mix(white, info, `${i}0%`)
    } else {
      colors[`--color-primary-light-${i}`] = mix(white, defaultTheme, `${i}0%`)
      colors[`--color-primary-white-${i}0\\%`] = mix(
        defaultTheme,
        white,
        `${i}0%`,
      )
      colors[`--color-info-white-${i}0\\%`] = mix(info, white, `${i}0%`)
      colors[`--color-success-white-${i}0\\%`] = mix(success, white, `${i}0%`)
      colors[`--color-warning-white-${i}0\\%`] = mix(warning, white, `${i}0%`)
      colors[`--color-danger-white-${i}0\\%`] = mix(warning, white, `${i}0%`)
      colors[`--color-black-primary-${i}0\\%`] = mix(
        black,
        defaultTheme,
        `${i}0%`,
      )
      colors[`--color-black-success-${i}0\\%`] = mix(black, success, `${i}0%`)
      colors[`--color-black-warning-${i}0\\%`] = mix(black, warning, `${i}0%`)
      colors[`--color-black-danger-${i}0\\%`] = mix(black, danger, `${i}0%`)
      colors[`--color-black-info-${i}0\\%`] = mix(black, info, `${i}0%`)
      colors[`--color-white-primary-${i}0\\%`] = mix(
        white,
        defaultTheme,
        `${i}0%`,
      )
      colors[`--color-white-success-${i}0\\%`] = mix(white, success, `${i}0%`)
      colors[`--color-white-warning-${i}0\\%`] = mix(white, warning, `${i}0%`)
      colors[`--color-white-danger-${i}0\\%`] = mix(white, danger, `${i}0%`)
      colors[`--color-white-info-${i}0\\%`] = mix(white, info, `${i}0%`)
    }
  }
  return colors
}
