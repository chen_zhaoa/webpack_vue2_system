import Vue from 'vue'
import VueCookie from 'vue-cookie'
import moment from 'moment'
import App from '@S/App.vue'
import router from '@S/router'
import storeInit from '@S/store'
import ElementUI from 'element-ui'
import VueI18n from 'vue-i18n'
import httpRequest from '@S/utils/httpRequest'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
export function systemInit(params) {
  let store = storeInit(params ? params.storeModules : undefined)
  Vue.use(VueI18n)
  Vue.use(VueCookie)
  Vue.config.productionTip = false
  Vue.prototype.$http = httpRequest
  Vue.prototype.moment = moment
  let getI18n = new Promise((resolve) => {
    const messages = {
      en: {
        el: enLocale.el,
        中文: '切换中文',
      },
      zn: {
        el: zhLocale.el,
        中文: 'switching english',
      },
    }
    const i18n = new VueI18n({
      locale: 'zn',
      messages,
      fallbackLocale: 'zn',
      silentTranslationWarn: true,
    })
    resolve(i18n)
  })
  return new Promise((resolve) => {
    Promise.all([getI18n]).then((res) => {
      let i18n = res[0]
      Vue.use(ElementUI, {
        i18n: (key, value) => i18n.t(key, value),
      })
      resolve(
        new Vue({
          router,
          store,
          i18n,
          render: (h) => h(App),
        }),
      )
    })
  })
}
