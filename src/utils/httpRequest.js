import axios from 'axios'

// 超时时间
axios.defaults.timeout = 100000
axios.defaults.withCredentials = true
axios.defaults.headers['Content-Type'] =
  'application/x-www-form-urlencoded; charset=utf-8'
const BASE_URL =
  process.env.NODE_ENV !== 'production'
    ? process.env.VUE_APP_BASE_API
    : process.env.VUE_APP_SERVER_URL
// 对面暴露的基础请求路径
axios.BASE_URL = BASE_URL

/**
 * 请求拦截
 */
axios.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

/**
 * 响应拦截
 */
axios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    return Promise.reject(error)
  },
)

export default axios
